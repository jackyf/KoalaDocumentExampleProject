package org.openkoala.multipdsdemo.core.domain.secondds;

import static org.junit.Assert.*;

import javax.inject.Inject;

import org.dayatang.querychannel.QueryChannelService;
import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;
import org.springframework.test.context.transaction.TransactionConfiguration;

@TransactionConfiguration(transactionManager = "second_transactionManager",defaultRollback = true) 
public class AddressTest extends KoalaBaseSpringTestCase {

	@Inject
	private QueryChannelService second_queryChannel;

	@Test
	public void testRepository() {
		Address address = new Address();
		address.setCity("city");
		address.setCountry("country");
		address.setProvince("privince");
		address.save();
		
		assertEquals(address, Address.get(Address.class, address.getId()));
	}
	
	@Test
	public void testQueryChannel() {
		Address address = new Address();
		address.setCity("city");
		address.setCountry("country");
		address.setProvince("privince");
		address.save();
		
		assertTrue(second_queryChannel.createJpqlQuery("select a from Address a").setPage(0, 10).pagedList().getData().contains(address));
	}

}
