package org.openkoala.multipdsdemo.core.domain;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import javax.inject.Inject;

import org.dayatang.querychannel.QueryChannelService;
import org.junit.Test;
import org.openkoala.koala.util.KoalaBaseSpringTestCase;
import org.springframework.test.context.transaction.TransactionConfiguration;

@TransactionConfiguration(transactionManager = "transactionManager",defaultRollback = true) 
public class EmployeeTest extends KoalaBaseSpringTestCase {

	@Inject
	private QueryChannelService queryChannel;
	
	@Test
	public void testRepository() {
		Employee employee = new Employee();
		employee.setName("test-name");
		employee.setGender("man");
		employee.setAge(88);
		employee.save();
		
		assertEquals(employee, Employee.get(Employee.class, employee.getId()));
	}

	@Test
	public void testQueryChannel() {
		Employee employee = new Employee();
		employee.setName("test-name");
		employee.setGender("man");
		employee.setAge(88);
		employee.save();
		
		assertTrue(queryChannel.createJpqlQuery("select e from Employee e").setPage(0, 10).pagedList().getData().contains(employee));
	}

}
