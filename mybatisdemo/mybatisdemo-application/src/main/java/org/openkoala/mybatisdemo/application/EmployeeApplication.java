package org.openkoala.mybatisdemo.application;

import java.util.List;

import org.openkoala.mybatisdemo.core.domain.Employee;

public interface EmployeeApplication {

	public Employee getEmployee(Long id);

	public Employee saveEmployee(Employee employee);

	public void updateEmployee(Employee employee);

	public void removeEmployee(Long id);

	public void removeEmployees(Long[] ids);

	public List<Employee> findAllEmployee();

}
