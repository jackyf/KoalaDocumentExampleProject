package org.openkoala.mybatisdemo.facade.dto;

import java.io.Serializable;

public class EmployeeDTO implements Serializable {

	private static final long serialVersionUID = 2375391686089532586L;

	private Long id;
						
	private Integer age;
		
	private String name;
		
	private String gender;
							

	public void setAge(Integer age) { 
		this.age = age;
	}

	public Integer getAge() {
		return this.age;
	}
	
	public void setName(String name) { 
		this.name = name;
	}

	public String getName() {
		return this.name;
	}
	
	public void setGender(String gender) { 
		this.gender = gender;
	}

	public String getGender() {
		return this.gender;
	}
	

	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getId() {
		
		return id;
	}

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeDTO other = (EmployeeDTO) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}