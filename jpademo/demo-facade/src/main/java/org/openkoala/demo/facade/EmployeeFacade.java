package org.openkoala.demo.facade;

import java.util.List;
import org.dayatang.querychannel.Page;
import org.openkoala.demo.facade.dto.*;

public interface EmployeeFacade {

	public EmployeeDTO getEmployee(Long id);
	
	public EmployeeDTO saveEmployee(EmployeeDTO employee);
	
	public void updateEmployee(EmployeeDTO employee);
	
	public void removeEmployee(Long id);
	
	public void removeEmployees(Long[] ids);
	
	public List<EmployeeDTO> findAllEmployee();
	
	public Page<EmployeeDTO> pageQueryEmployee(EmployeeDTO employee, int currentPage, int pageSize);

	void assignEmployeeToOrganization(EmployeeDTO employeeDto, Long organizationId);
	
	List<EmployeeDTO> findEmployeesByAgeRange(Integer from, Integer to);

}

