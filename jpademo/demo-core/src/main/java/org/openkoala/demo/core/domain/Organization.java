package org.openkoala.demo.core.domain;
 
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.openkoala.koala.commons.domain.KoalaAbstractEntity;
 
@Entity
@Table(name = "organizations")
public class Organization extends KoalaAbstractEntity {
     
    /**
     * 
     */
    private static final long serialVersionUID = 1946346499573992799L;
 
    private String name;
     
    @Column(name = "serial_number")
    private String serialNumber;
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public String getSerialNumber() {
        return serialNumber;
    }
 
    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }
 
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
        return result;
    }
 
    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Organization other = (Organization) obj;
        if (getId() == null) {
            if (other.getId() != null)
                return false;
        } else if (!getId().equals(other.getId()))
            return false;
        return true;
    }
 
    @Override
    public String toString() {
        return name;
    }
	@Override
	public String[] businessKeys() {
		// TODO Auto-generated method stub
		return null;
	}
}
